# -*- coding: utf-8 -*-
"""Connector between the main energy meter at The Green Village and Kafka.

The data from the SINEAX AM3000 energy meter is read out over Modbus TCP
connection and sent to the Kafka REST Proxy.
There is no separation into high tariff and low tarrif. Therefore, the device
only measures high tariff. All low tariff measurements are zero.

SINEAX AM3000 documentation:
https://www.arc.ro/userfiles/docs/Camille%20Bauer/SINEAX%20AM3000%20OPERATING%20INSTRUCTIONS.pdf
https://www.arc.ro/userfiles/docs/Camille%20Bauer/SINEAX%20AM3000%20INTERFACE%20DOCUMENTATION%20MODBUS%20TCP.pdf

pyModbus documentation:
https://pymodbus.readthedocs.io/en/v2.1.0/index.html
"""

import logging
import pathlib
import time
from configparser import ConfigParser

import requests
from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from pymodbus.payload import BinaryPayloadDecoder
from requests.exceptions import RequestException

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__file__)

# Quantities to read
measurements = [
    # {"name": "U", "description": "System voltage", "unit": "V", "address": 100},
    # {"name": "A", "description": "System current", "unit": "A", "address": 116},
    {"name": "P", "description": "Active power system", "unit": "W", "address": 126},
    {
        "name": "Q",
        "description": "Reactive power system",
        "unit": "var",
        "address": 134,
    },
    {"name": "S", "description": "Apparent power system", "unit": "VA", "address": 142},
    {"name": "F", "description": "System frequency", "unit": "Hz", "address": 150},
    {
        "name": "P_I_IV_HT",
        "description": "Active energy QI+IV, high tarrif",
        "unit": "Wh",
        "address": 4100,
    },
    {
        "name": "P_II_III_HT",
        "description": "Active energy QII+III, high tarrif",
        "unit": "Wh",
        "address": 4102,
    },
    {
        "name": "Q_I_II_HT",
        "description": "Reactive energy QI+II, high tarrif",
        "unit": "varh",
        "address": 4104,
    },
    {
        "name": "Q_III_IV_HT",
        "description": "Reactive energy QIII+IV, high tarrif",
        "unit": "varh",
        "address": 4106,
    },
]

if __name__ == "__main__":
    # Read config file.
    parent_dir = pathlib.Path(__file__).parent.resolve()
    config = ConfigParser()
    config.read(str(parent_dir / "hvi.cfg"))
    modbus_host = config.get("Modbus", "host")
    modbus_port = config.getint("Modbus", "port")
    rest_url = config.get("RestProxy", "url")
    rest_username = config.get("RestProxy", "username")
    rest_password = config.get("RestProxy", "password")
    registry_url = config.get("RestProxy", "schema_registry_url")

    # Get schema from the schema registry.
    r = requests.get(registry_url, timeout=5)
    logger.debug(r.text)
    logger.info(f"Kafka Schema Registry response code: {r.status_code}")
    schema_id = r.json()["id"]
    schema = r.json()["schema"]

    client = ModbusClient(modbus_host, port=modbus_port)

    while True:
        # Open or reconnect TCP to server.
        if not client.is_socket_open():
            if not client.connect():
                logger.warning(f"Unable to connect to {modbus_host}:{modbus_port}")
                time.sleep(5)
                continue

        values = []
        for m in measurements:
            # It is necessary to address the registers with offset -1.
            address = m["address"] - 1
            res = client.read_holding_registers(address, count=2)
            logger.debug(f"Registers at address {address}: {res.registers}")
            decoder = BinaryPayloadDecoder.fromRegisters(
                res.registers, byteorder=">", wordorder="<"
            )
            decoded = decoder.decode_32bit_float()
            logger.debug(f"Decoded value: {decoded}")
            value = {
                "name": m["name"],
                "description": m["description"],
                "unit": m["unit"],
                "type": "FLOAT",
                "value": {"float": decoded},
            }
            logger.debug(value)
            values.append(value)

        value = {
            "metadata": {
                "project_id": "hvi",
                "device_id": "hvi",
                "description": "",
                "type": {"string": "SINEAX AM3000"},
                "manufacturer": {"null": None},
                "serial": {"null": None},
                "placement_timestamp": {"null": None},
                "location": {"null": None},
                "latitude": {"null": None},
                "longitude": {"null": None},
                "altitude": {"null": None},
            },
            "data": {
                "project_id": "hvi",
                "device_id": "hvi",
                "timestamp": int(time.time() * 1e3),  # time in ms
                "values": values,
            },
        }

        data = {
            "value_schema_id": schema_id,
            # "value_schema": schema,
            "records": [{"value": value}],
        }
        logger.debug(data)

        try:
            headers = {
                "Content-Type": "application/vnd.kafka.avro.v2+json",
                "Accept": "application/vnd.kafka.v2+json",
            }
            r = requests.post(
                rest_url,
                json=data,
                headers=headers,
                auth=(rest_username, rest_password),
                timeout=5,
            )
            logger.debug(r.text)
            logger.info(f"Kafka REST Proxy response code: {r.status_code}")

        except RequestException as e:
            logger.error(e)
            continue

        time.sleep(1)
