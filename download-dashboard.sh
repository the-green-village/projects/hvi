#!/bin/bash
curl https://$PROJECT_NAME-grafana.green-village.sda-projects.nl/api/dashboards/uid/mJ096wEiz -u "$PROJECT_NAME:$PROJECT_PASSWORD" | jq . > dashboard.json

# When importing dashboards, the id field should be removed as it is an auto-incrementing numeric value unique per Grafana install.
# For dashboard provisioning, the unique identifired `uid` is important when syncing dashboards amonng multiple Grafana installs.
# https://grafana.com/docs/http_api/dashboard/#identifier-id-vs-unique-identifier-uid

# Remove the id field from the JSON file.
jq '.dashboard.id=null' dashboard.json | sponge dashboard.json
# Metadata can be also removed.
jq 'del(.meta)' dashboard.json | sponge dashboard.json
