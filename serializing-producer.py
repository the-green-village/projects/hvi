# -*- coding: utf-8 -*-
"""Connector between the main energy meter at The Green Village and Kafka.

The data from the SINEAX AM3000 energy meter is read out over Modbus TCP
connection and sent using SerializingProducer.
There is no separation into high tariff and low tarrif. Therefore, the device
only measures high tariff. All low tariff measurements are zero.

SINEAX AM3000 documentation:
https://www.arc.ro/userfiles/docs/Camille%20Bauer/SINEAX%20AM3000%20OPERATING%20INSTRUCTIONS.pdf
https://www.arc.ro/userfiles/docs/Camille%20Bauer/SINEAX%20AM3000%20INTERFACE%20DOCUMENTATION%20MODBUS%20TCP.pdf

pyModbus documentation:
https://pymodbus.readthedocs.io/en/v2.1.0/index.html
"""

import logging
import pathlib
import time
from configparser import ConfigParser

from confluent_kafka import SerializingProducer
from confluent_kafka.schema_registry import SchemaRegistryClient
from confluent_kafka.schema_registry.avro import AvroSerializer
from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from pymodbus.payload import BinaryPayloadDecoder

# Quantities to read
_measurements = [
    # {"measurement_id": "U", "measurement_description": "System voltage", "unit": "V", "address": 100},
    # {"measurement_id": "A", "measurement_description": "System current", "unit": "A", "address": 116},
    {
        "measurement_id": "P",
        "measurement_description": "Active power system",
        "unit": "W",
        "address": 126,
    },
    {
        "measurement_id": "Q",
        "measurement_description": "Reactive power system",
        "unit": "var",
        "address": 134,
    },
    {
        "measurement_id": "S",
        "measurement_description": "Apparent power system",
        "unit": "VA",
        "address": 142,
    },
    {
        "measurement_id": "F",
        "measurement_description": "System frequency",
        "unit": "Hz",
        "address": 150,
    },
    {
        "measurement_id": "P_I_IV_HT",
        "measurement_description": "Active energy QI+IV, high tarrif",
        "unit": "Wh",
        "address": 4100,
    },
    {
        "measurement_id": "P_II_III_HT",
        "measurement_description": "Active energy QII+III, high tarrif",
        "unit": "Wh",
        "address": 4102,
    },
    {
        "measurement_id": "Q_I_II_HT",
        "measurement_description": "Reactive energy QI+II, high tarrif",
        "unit": "varh",
        "address": 4104,
    },
    {
        "measurement_id": "Q_III_IV_HT",
        "measurement_description": "Reactive energy QIII+IV, high tarrif",
        "unit": "varh",
        "address": 4106,
    },
]


if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        level=logging.INFO,
    )
    logger = logging.getLogger(__file__)

    # Read config file.
    parent_dir = pathlib.Path(__file__).parent.resolve()
    config = ConfigParser()
    config.read(str(parent_dir / "hvi.cfg"))
    modbus_host = config.get("Modbus", "host")
    modbus_port = config.getint("Modbus", "port")
    bootstrap_servers = config.get("Kafka", "bootstrap_servers")
    api_key = config.get("Kafka", "api_key")
    api_secret = config.get("Kafka", "api_secret")
    topic = config.get("Kafka", "topic")
    schema_registry_url = config.get("Kafka", "schema_registry_url")
    schema_registry_auth = config.get("Kafka", "schema_registry_auth")

    conf = {"url": schema_registry_url, "basic.auth.user.info": schema_registry_auth}
    schema_registry_client = SchemaRegistryClient(conf)

    schema_str = '{"type":"record","name":"GreenVillageRecord","namespace":"thegreenvillage","fields":[{"name":"project_id","type":"string","doc":"Globally unique id for the project."},{"name":"application_id","type":"string","doc":"Unique id for the application/use case."},{"name":"device_id","type":"string","doc":"Unique id for the device."},{"name":"timestamp","type":"long","doc":"Timestamp (milliseconds since unix epoch) of the measurement."},{"name":"measurements","type":{"type":"array","items":{"type":"record","name":"measurement","fields":[{"name":"measurement_id","type":"string","doc":"Unique id for the measurement."},{"name":"value","type":["null","boolean","int","long","float","double","string"],"doc":"Measured value."},{"name":"unit","type":["null","string"],"doc":"Unit of the measurement.","default":null},{"name":"measurement_description","type":["null","string"],"doc":"Measurement description.","default":null}]}},"doc":"Array of measurements."},{"name":"project_description","type":["null","string"],"doc":"Project description.","default":null},{"name":"application_description","type":["null","string"],"doc":"Application/use case description.","default":null},{"name":"device_description","type":["null","string"],"doc":"Device description.","default":null},{"name":"device_manufacturer","type":["null","string"],"doc":"Device manufacturer.","default":null},{"name":"device_type","type":["null","string"],"doc":"Device type.","default":null},{"name":"device_serial","type":["null","string"],"doc":"Device serial number.","default":null},{"name":"location_id","type":["null","string"],"doc":"Unique id for the location.","default":null},{"name":"location_description","type":["null","string"],"doc":"Location description.","default":null},{"name":"latitude","type":["null","float"],"doc":"Latitude in decimal degrees of the device location (GPS coordinates).","default":null},{"name":"longitude","type":["null","float"],"doc":"Longitude in decimal degrees of the device location (GPS coordinates).","default":null},{"name":"altitude","type":["null","float"],"doc":"Altitude in meters above the mean sea level.","default":null}]}'
    conf = {"auto.register.schemas": False}
    avro_serializer = AvroSerializer(schema_registry_client, schema_str, conf=conf)

    # Create Producer instance
    producer = SerializingProducer(
        {
            "client.id": "hvi-producer",
            "bootstrap.servers": bootstrap_servers,
            "sasl.mechanisms": "PLAIN",
            "security.protocol": "SASL_SSL",
            "sasl.username": api_key,
            "sasl.password": api_secret,
            "ssl.ca.location": "/etc/ssl/certs/ca-certificates.crt",
            "value.serializer": avro_serializer,
            "acks": "1",
            # If you don’t care about duplicates and ordering:
            "retries": 10000000,
            "delivery.timeout.ms": 2147483647,
            "max.in.flight.requests.per.connection": 5
            #        # If you don’t care about duplicates but care about ordering:
            #        'retries': 10000000,
            #        'delivery.timeout.ms': 2147483647,
            #        'max.in.flight.requests.per.connection': 1
            #        # If you care about duplicates and ordering:
            #        'retries': 10000000,
            #        'delivery.timeout.ms': 2147483647,
            #        'enable.idempotence': True
        }
    )

    delivered_records = 0

    # Optional per-message on_delivery handler (triggered by poll() or flush())
    # when a message has been successfully delivered or
    # permanently failed delivery (after retries).
    def acked(err, msg):
        global delivered_records
        """Delivery report handler called on
        successful or failed delivery of message
        """
        if err is not None:
            print("Failed to deliver message: {}".format(err))
        else:
            delivered_records += 1
            print(
                "Produced record to topic {} partition [{}] @ offset {}".format(
                    msg.topic(), msg.partition(), msg.offset()
                )
            )

    client = ModbusClient(modbus_host, port=modbus_port)

    while True:
        # Open or reconnect TCP to server.
        if not client.is_socket_open():
            if not client.connect():
                logger.warning(f"Unable to connect to {modbus_host}:{modbus_port}")
                time.sleep(5)
                continue

        measurements = []
        for m in _measurements:
            # It is necessary to address the registers with offset -1.
            address = m["address"] - 1
            res = client.read_holding_registers(address, count=2)
            logger.debug(f"Registers at address {address}: {res.registers}")
            decoder = BinaryPayloadDecoder.fromRegisters(
                res.registers, byteorder=">", wordorder="<"
            )
            decoded = decoder.decode_32bit_float()
            logger.debug(f"Decoded value: {decoded}")
            measurement = {
                "measurement_id": m["measurement_id"],
                "measurement_description": m["measurement_description"],
                "unit": m["unit"],
                "value": decoded,
            }
            logger.debug(measurement)
            measurements.append(measurement)

        value = {
            "project_id": "hvi",
            "application_id": "hvi",
            "device_id": "hvi",
            "type": "SINEAX AM3000",
            "timestamp": int(time.time() * 1e3),  # time in ms
            "measurements": measurements,
        }

        producer.produce(topic=topic, value=value, on_delivery=acked)
        producer.poll(0)

        time.sleep(1)

    producer.flush()

    print("{} messages were produced to topic {}!".format(delivered_records, topic))
