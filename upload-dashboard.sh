#!/bin/bash

# Add folder id.
export FOLDER_ID=$(curl https://$PROJECT_NAME-grafana.green-village.sda-projects.nl/api/folders -u "$PROJECT_NAME:$PROJECT_PASSWORD" | jq '.[] | select(.title == "'$PROJECT_NAME'") | .id' | tr -d '"')
jq ".folderId=$FOLDER_ID" dashboard.json | sponge dashboard.json

# Upload the dashboard.
curl -XPOST \
  -H "Accept: application/json" -H "Content-Type: application/json" \
  -u "$PROJECT_NAME:$PROJECT_PASSWORD" \
  https://$PROJECT_NAME-grafana.green-village.sda-projects.nl/api/dashboards/db \
  --data @dashboard.json | jq .
