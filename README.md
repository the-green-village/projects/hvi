# HVI

The data from the **SINEAX AM3000** energy meter is read out over **Modbus TCP** connection from a Raspberry Pi.

SINEAX AM3000 documentation:
- [Modbus_interface_AMx000_19-01.pdf](Modbus_interface_AMx000_19-01.pdf)
- [sineax-am3000-sb_gb_modbus-tcp.pdf](sineax-am3000-sb_gb_modbus-tcp.pdf)
https://www.arc.ro/userfiles/docs/Camille%20Bauer/SINEAX%20AM3000%20INTERFACE%20DOCUMENTATION%20MODBUS%20TCP.pdf
- [sineax_am3000_operating_instructions.pdf](sineax_am3000_operating_instructions.pdf)
https://www.arc.ro/userfiles/docs/Camille%20Bauer/SINEAX%20AM3000%20OPERATING%20INSTRUCTIONS.pdf

Two python scripts are available in this repository:
- [serializing-producer.py](serializing-producer.py) sends data using a Kafka producer (preferred way, more performant),
- [modbus-rest.py](modbus-rest.py) sends data to the Kafka REST Proxy.

## Tests

### mbpoll

`mbpoll` is a command line utility to communicate with Modbus slave.

https://github.com/epsilonrt/mbpoll

```
docker build -t mbpoll .
docker run -it mbpoll bash
```

```
mbpoll -h

# -0            First reference is 0 (PDU addressing) instead 1
mbpoll -1 -r 4100 -0 -c 2 -p 502 192.168.1.101

# -t 4:float    32-bit float data type in output (holding) register table
mbpoll -1 -r 4100 -t 4:float -p 502 192.168.1.101
```

### PyModbus

https://pymodbus.readthedocs.io/en/v2.1.0/index.html

```
from pymodbus.client.sync import ModbusTcpClient as ModbusClient
client = ModbusClient("192.168.1.101", port=502)
client.connect()
result = client.read_holding_registers(4100 - 1, count=2)
print(result.registers)
```

## Raspberry Pi

### ZeroTier

The HVI meter is read from a Raspberry Pi. The Raspberry Pi can be accessed through ZeroTier.

```
ssh pi@10.242.233.253
```

### confluent-kafka and librdkakfa

The `confluent-kafka` python package needs `librdkafka`.

http://mirrordirector.raspbian.org/raspbian/pool/main/libr/librdkafka/

```
sudo apt-get install libssl-dev libsasl2-dev

wget http://mirrordirector.raspbian.org/raspbian/pool/main/libr/librdkafka/librdkafka_1.8.0.orig.tar.gz
tar xvzf librdkafka_1.8.0.orig.tar.gz
cd librdkafka-1.8.0
./configure
make
sudo make install

pip install -r requirements.txt
```

### rc.local

Edit `/etc/rc.local` to run the python script at startup.

```
sudo -H -u pi /usr/bin/python3.7 /home/pi/hvi/serializing-producer.py 1>/home/pi/hvi/serializing-producer.log 2>&1 &
```
