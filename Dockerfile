FROM ubuntu:19.10

RUN apt-get update && apt-get install -y software-properties-common wget && \
    wget -O- http://www.piduino.org/piduino-key.asc | apt-key add - && \
    add-apt-repository 'deb http://apt.piduino.org stretch piduino' && \
    apt update && apt install -y mbpoll
